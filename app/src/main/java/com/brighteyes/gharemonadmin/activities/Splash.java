package com.brighteyes.gharemonadmin.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;


import com.brighteyes.gharemonadmin.R;
import com.brighteyes.gharemonadmin.util.CommonUtil;

import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by bridge on 11/20/2016.
 */

public class Splash extends Activity {
    private boolean isInBackGround = false;
    /**
     * Duration of wait
     **/
    private final int SPLASH_DISPLAY_LENGTH = 3000;

//    public void onAttachedToWindow() {
//        super.onAttachedToWindow();
//        Window window = getWindow();
//        window.setFormat(PixelFormat.RGBA_8888);
//    }
//
//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonUtil.changeLocale(this, new Locale("ar"));
        setContentView(R.layout.splash);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isInBackGround = false;
        CommonUtil.changeLocale(this, new Locale("ar"));
        if (!isInBackGround) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                    Intent mainIntent = new Intent(Splash.this, CountryActivity.class);
                    Splash.this.startActivity(mainIntent);
                    Splash.this.finish();
                }
            }, SPLASH_DISPLAY_LENGTH);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isInBackGround = true;
    }
}