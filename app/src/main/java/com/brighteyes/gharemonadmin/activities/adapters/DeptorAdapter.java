package com.brighteyes.gharemonadmin.activities.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.brighteyes.gharemonadmin.R;
import com.brighteyes.gharemonadmin.activities.entities.Deptor;
import com.brighteyes.gharemonadmin.util.Singleton;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by AmrNabil on 5/20/2017.
 */

public class DeptorAdapter extends RecyclerView.Adapter<DeptorAdapter.MyViewHolder> {
    private Context mContext;

    public DeptorAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public DeptorAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.deptor_cell_item, parent, false);

        return new DeptorAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DeptorAdapter.MyViewHolder holder, int position) {
        final Deptor deptor = Singleton.deptorList.get(position);
        holder.name.setText(deptor.getName());
        holder.cityName.setText(deptor.getCityName());
        holder.refrencePhone.setText(deptor.getRefrencePhone());
        holder.value.setText(deptor.getValue());
        holder.station.setText(deptor.getStation());
        holder.paid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase.getInstance().getReference("deptor").child(Singleton.selectedCity.getKey()).child(deptor.getKey()).child("active").setValue(false);
            }
        });

    }

    @Override
    public int getItemCount() {
        return Singleton.deptorList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.deptor_name)
        TextView name;

        @Bind(R.id.dept_value)
        TextView value;

        @Bind(R.id.station_locker)
        TextView station;

        @Bind(R.id.refrence_phone)
        TextView refrencePhone;

        @Bind(R.id.city_name)
        TextView cityName;

        @Bind(R.id.paid)
        Button paid;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
        }
    }
}
