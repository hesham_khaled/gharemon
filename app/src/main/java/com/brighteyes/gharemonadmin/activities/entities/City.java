package com.brighteyes.gharemonadmin.activities.entities;

/**
 * Created by AmrNabil on 5/19/2017.
 */

public class City {
    private String key;
    private String name;
    private String flag;

    public City() {
    }

    public City(String key, String name, String flag) {
        this.key = key;
        this.name = name;
        this.flag = flag;
    }

    public City(String name, String flag) {
        this.name = name;
        this.flag = flag;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
