package com.brighteyes.gharemonadmin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.brighteyes.gharemonadmin.R;
import com.brighteyes.gharemonadmin.activities.adapters.CityAdapter;
import com.brighteyes.gharemonadmin.activities.entities.City;
import com.brighteyes.gharemonadmin.activities.menuPages.AboutCompanyActivity;
import com.brighteyes.gharemonadmin.activities.menuPages.AboutProgramActivity;
import com.brighteyes.gharemonadmin.activities.menuPages.ContactUsActivity;
import com.brighteyes.gharemonadmin.util.CommonUtil;
import com.brighteyes.gharemonadmin.util.Singleton;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CityActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private CityAdapter mAdapter;

    public InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        try {
            Singleton.cityList = new ArrayList<>();
            recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
            final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressLoading);
            progressBar.setVisibility(View.VISIBLE);
            if (CommonUtil.isInternetAvailable(this)) {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("city").child(Singleton.selectedCountry.getKey());
                ref.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        City city = new City();
                        city.setFlag(dataSnapshot.child("flag").getValue().toString());
                        city.setName(dataSnapshot.child("name").getValue().toString());
                        city.setKey(dataSnapshot.getKey());
                        Singleton.cityList.add(city);
                        mAdapter.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        for (City city : Singleton.cityList) {
                            if (city.getKey().equals(dataSnapshot.getKey())) {
                                city.setFlag(dataSnapshot.child("flag").getValue().toString());
                                city.setName(dataSnapshot.child("name").getValue().toString());
                                break;
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        List<City> countries = Singleton.cityList;
                        for (City city : countries) {
                            if (city.getKey().equals(dataSnapshot.getKey())) {
                                Singleton.cityList.remove(city);
                                break;
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                mAdapter = new CityAdapter(this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mAdapter);
            } else {
                Toast.makeText(this, getResources().getString(R.string.no_intenret), Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, getResources().getString(R.string.no_intenret), Toast.LENGTH_SHORT).show();
        } catch (InterruptedException e) {
            Toast.makeText(this, getResources().getString(R.string.no_intenret), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.fullScreenAdId));

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });

        requestNewInterstitial();
    }

    public void showFullScreenAd() {
        mInterstitialAd.show();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        mInterstitialAd.loadAd(adRequest);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about_program:
                if (!getClass().equals(AboutProgramActivity.class)) {
                    Intent intent = new Intent(this, AboutProgramActivity.class);
                    startActivity(intent);
                }
                return true;
            case R.id.about_company:
                if (!getClass().equals(AboutCompanyActivity.class)) {
                    Intent intent = new Intent(this, AboutCompanyActivity.class);
                    startActivity(intent);
                }
                return true;
            case R.id.contact_us:
                if (!getClass().equals(ContactUsActivity.class)) {
                    Intent intent = new Intent(this, ContactUsActivity.class);
                    startActivity(intent);
                }
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}

