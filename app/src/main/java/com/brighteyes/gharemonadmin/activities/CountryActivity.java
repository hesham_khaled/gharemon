package com.brighteyes.gharemonadmin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.brighteyes.gharemonadmin.R;
import com.brighteyes.gharemonadmin.activities.adapters.CountryAdapter;
import com.brighteyes.gharemonadmin.activities.entities.Country;
import com.brighteyes.gharemonadmin.activities.menuPages.AboutCompanyActivity;
import com.brighteyes.gharemonadmin.activities.menuPages.AboutProgramActivity;
import com.brighteyes.gharemonadmin.activities.menuPages.ContactUsActivity;
import com.brighteyes.gharemonadmin.util.CommonUtil;
import com.brighteyes.gharemonadmin.util.Singleton;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CountryActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private CountryAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        try {
            Singleton.countryList = new ArrayList<>();
            recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
            final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressLoading);
            progressBar.setVisibility(View.VISIBLE);
            if (CommonUtil.isInternetAvailable(this)) {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("country");
                ref.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Country country = new Country();
                        country.setFlag(dataSnapshot.child("flag").getValue().toString());
                        country.setName(dataSnapshot.child("name").getValue().toString());
                        country.setKey(dataSnapshot.getKey());
                        Singleton.countryList.add(country);
                        mAdapter.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        for (Country country : Singleton.countryList) {
                            if (country.getKey().equals(dataSnapshot.getKey())) {
                                country.setFlag(dataSnapshot.child("flag").getValue().toString());
                                country.setName(dataSnapshot.child("name").getValue().toString());
                                break;
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        List<Country> countries = Singleton.countryList;
                        for (Country country : countries) {
                            if (country.getKey().equals(dataSnapshot.getKey())) {
                                Singleton.countryList.remove(country);
                                break;
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                mAdapter = new CountryAdapter(this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mAdapter);
            } else {
                Toast.makeText(this, getResources().getString(R.string.no_intenret), Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, getResources().getString(R.string.no_intenret), Toast.LENGTH_SHORT).show();
        } catch (InterruptedException e) {
            Toast.makeText(this, getResources().getString(R.string.no_intenret), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about_program:
                if (!getClass().equals(AboutProgramActivity.class)) {
                    Intent intent = new Intent(this, AboutProgramActivity.class);
                    startActivity(intent);
                }
                return true;
            case R.id.about_company:
                if (!getClass().equals(AboutCompanyActivity.class)) {
                    Intent intent = new Intent(this, AboutCompanyActivity.class);
                    startActivity(intent);
                }
                return true;
            case R.id.contact_us:
                if (!getClass().equals(ContactUsActivity.class)) {
                    Intent intent = new Intent(this, ContactUsActivity.class);
                    startActivity(intent);
                }
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
