package com.brighteyes.gharemonadmin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.brighteyes.gharemonadmin.R;
import com.brighteyes.gharemonadmin.activities.adapters.DeptorAdapter;
import com.brighteyes.gharemonadmin.activities.entities.Deptor;
import com.brighteyes.gharemonadmin.util.CommonUtil;
import com.brighteyes.gharemonadmin.util.Singleton;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GharemonActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private DeptorAdapter mAdapter;

    @Bind(R.id.adView)
    RelativeLayout adContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gharemon);
        ButterKnife.bind(this);

        loadBannerAd();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        try {
            Singleton.deptorList = new ArrayList<>();
            recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
            final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressLoading);
            progressBar.setVisibility(View.VISIBLE);
            if (CommonUtil.isInternetAvailable(this)) {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("deptor").child(Singleton.selectedCity.getKey());
                ref.orderByChild("active").equalTo(true).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Deptor deptor = new Deptor();
                        deptor.setName(dataSnapshot.child("name").getValue().toString());
                        deptor.setValue(dataSnapshot.child("value").getValue().toString());
                        deptor.setCountryName(dataSnapshot.child("countryName").getValue().toString());
                        deptor.setCountryKey(dataSnapshot.child("countryKey").getValue().toString());
                        deptor.setActive(true);
                        deptor.setCityKey(dataSnapshot.child("cityKey").getValue().toString());
                        deptor.setCityName(dataSnapshot.child("cityName").getValue().toString());
                        deptor.setStation(dataSnapshot.child("station").getValue().toString());
                        deptor.setRefrencePhone(dataSnapshot.child("refrencePhone").getValue().toString());
                        deptor.setKey(dataSnapshot.getKey());
                        Singleton.deptorList.add(deptor);
                        mAdapter.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        for (Deptor deptor : Singleton.deptorList) {
                            if (deptor.getKey().equals(dataSnapshot.getKey())) {
                                deptor.setName(dataSnapshot.child("name").getValue().toString());
                                deptor.setValue(dataSnapshot.child("value").getValue().toString());
                                deptor.setCountryName(dataSnapshot.child("countryName").getValue().toString());
                                deptor.setCountryKey(dataSnapshot.child("countryKey").getValue().toString());
                                deptor.setActive(true);
                                deptor.setCityKey(dataSnapshot.child("cityKey").getValue().toString());
                                deptor.setCityName(dataSnapshot.child("cityName").getValue().toString());
                                deptor.setStation(dataSnapshot.child("station").getValue().toString());
                                deptor.setRefrencePhone(dataSnapshot.child("refrencePhone").getValue().toString());
                                deptor.setKey(dataSnapshot.getKey());
                                break;
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        List<Deptor> deptors = Singleton.deptorList;
                        for (Deptor deptor : deptors) {
                            if (deptor.getKey().equals(dataSnapshot.getKey())) {
                                Singleton.deptorList.remove(deptor);
                                break;
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                mAdapter = new DeptorAdapter(this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mAdapter);
            } else {
                Toast.makeText(this, getResources().getString(R.string.no_intenret), Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, getResources().getString(R.string.no_intenret), Toast.LENGTH_SHORT).show();
        } catch (InterruptedException e) {
            Toast.makeText(this, getResources().getString(R.string.no_intenret), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GharemonActivity.this, DeptDialogActivity.class);
                startActivity(intent);
            }
        });
    }

    private void loadBannerAd() {
        AdRequest adRequest = new AdRequest.Builder().build();

        AdView mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(getResources().getString(R.string.bannerId));
        adContainer.addView(mAdView);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.d("TAG", "ad loaded");
            }
        });
        mAdView.loadAd(adRequest);
    }
}
