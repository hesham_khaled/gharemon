package com.brighteyes.gharemonadmin.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brighteyes.gharemonadmin.R;
import com.brighteyes.gharemonadmin.activities.CityActivity;
import com.brighteyes.gharemonadmin.activities.entities.Country;
import com.brighteyes.gharemonadmin.util.Singleton;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by AmrNabil on 5/18/2017.
 */

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.MyViewHolder> {
    private Context mContext;

    public CountryAdapter(Context context) {

        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_cell_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Country country = Singleton.countryList.get(position);
        holder.name.setText(country.getName());
        Picasso.with(mContext)
                .load(country.getFlag())
                .placeholder(R.mipmap.splash)
                .into(holder.flag);
        holder.containerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Singleton.selectedCountry = country;
                Intent intent = new Intent(mContext, CityActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return Singleton.countryList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.name)
        TextView name;

        @Bind(R.id.logo)
        ImageView flag;

        @Bind(R.id.mainCardView)
        CardView containerView;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
