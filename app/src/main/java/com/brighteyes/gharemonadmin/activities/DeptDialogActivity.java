package com.brighteyes.gharemonadmin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.brighteyes.gharemonadmin.R;
import com.brighteyes.gharemonadmin.activities.entities.Deptor;
import com.brighteyes.gharemonadmin.activities.menuPages.AboutCompanyActivity;
import com.brighteyes.gharemonadmin.activities.menuPages.AboutProgramActivity;
import com.brighteyes.gharemonadmin.activities.menuPages.ContactUsActivity;
import com.brighteyes.gharemonadmin.util.Singleton;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class DeptDialogActivity extends AppCompatActivity {
    private EditText name;
    private EditText value;
    private EditText station;
    private EditText refrencePhone;
    private Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gharem_input);
        name = (EditText) findViewById(R.id.name);
        value = (EditText) findViewById(R.id.value);
        station = (EditText) findViewById(R.id.station);
        refrencePhone = (EditText) findViewById(R.id.refrence_phone);
        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("deptor").child(Singleton.selectedCity.getKey());
                String key = ref.push().getKey();
                Deptor deptor = new Deptor(key, name.getText().toString(), value.getText().toString(), station.getText().toString(), refrencePhone.getText().toString(), Singleton.selectedCountry.getKey(), Singleton.selectedCountry.getName(), Singleton.selectedCity.getKey(), Singleton.selectedCity.getName(), true);
                Map<String, Object> map = new HashMap<String, Object>();
                map.put(key, deptor);
                ref.updateChildren(map).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(DeptDialogActivity.this, getResources().getString(R.string.saved_successfully), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about_program:
                if (!getClass().equals(AboutProgramActivity.class)) {
                    Intent intent = new Intent(this, AboutProgramActivity.class);
                    startActivity(intent);
                }
                return true;
            case R.id.about_company:
                if (!getClass().equals(AboutCompanyActivity.class)) {
                    Intent intent = new Intent(this, AboutCompanyActivity.class);
                    startActivity(intent);
                }
                return true;
            case R.id.contact_us:
                if (!getClass().equals(ContactUsActivity.class)) {
                    Intent intent = new Intent(this, ContactUsActivity.class);
                    startActivity(intent);
                }
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
