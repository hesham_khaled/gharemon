package com.brighteyes.gharemonadmin.activities.menuPages;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.brighteyes.gharemonadmin.R;

public class AboutCompanyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_company);
    }
}
