package com.brighteyes.gharemonadmin.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brighteyes.gharemonadmin.R;
import com.brighteyes.gharemonadmin.activities.CityActivity;
import com.brighteyes.gharemonadmin.activities.GharemonActivity;
import com.brighteyes.gharemonadmin.activities.entities.City;
import com.brighteyes.gharemonadmin.util.Singleton;
import com.squareup.picasso.Picasso;

/**
 * Created by AmrNabil on 5/19/2017.
 */

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.MyViewHolder> {
    private Context mContext;

    public CityAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public CityAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_cell_item, parent, false);

        return new CityAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CityAdapter.MyViewHolder holder, int position) {
        final City city = Singleton.cityList.get(position);
        holder.name.setText(city.getName());
        Picasso.with(mContext)
                .load(city.getFlag())
                .placeholder(R.mipmap.splash)
                .fit()
                .into(holder.flag);
        holder.containerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((CityActivity) mContext).showFullScreenAd();

                Singleton.selectedCity = city;
                Intent intent = new Intent(mContext, GharemonActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return Singleton.cityList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView flag;
        public View containerView;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            flag = (ImageView) view.findViewById(R.id.logo);
            containerView = view;
        }
    }


}
