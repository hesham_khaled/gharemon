package com.brighteyes.gharemonadmin.activities.entities;

/**
 * Created by AmrNabil on 5/20/2017.
 */

public class Deptor {
    private String key;
    private String name;
    private String value;
    private String station;
    private String refrencePhone;
    private String countryKey;
    private String countryName;
    private String cityKey;
    private String cityName;
    private boolean active;

    public Deptor() {

    }

    public Deptor(String key, String name, String value, String station, String refrencePhone, String countryKey, String countryName, String cityKey, String cityName, boolean active) {
        this.key = key;
        this.name = name;
        this.value = value;
        this.station = station;
        this.refrencePhone = refrencePhone;
        this.countryKey = countryKey;
        this.countryName = countryName;
        this.cityKey = cityKey;
        this.cityName = cityName;
        this.active = active;
    }

    public Deptor(String name, String value, String station, String refrencePhone, String countryKey, String countryName, String cityKey, String cityName, boolean active) {
        this.name = name;
        this.value = value;
        this.station = station;
        this.refrencePhone = refrencePhone;
        this.countryKey = countryKey;
        this.countryName = countryName;
        this.cityKey = cityKey;
        this.cityName = cityName;
        this.active = active;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getRefrencePhone() {
        return refrencePhone;
    }

    public void setRefrencePhone(String refrencePhone) {
        this.refrencePhone = refrencePhone;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCountryKey() {
        return countryKey;
    }

    public void setCountryKey(String countryKey) {
        this.countryKey = countryKey;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCityKey() {
        return cityKey;
    }

    public void setCityKey(String cityKey) {
        this.cityKey = cityKey;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
