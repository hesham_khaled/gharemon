package com.brighteyes.gharemonadmin.util;


import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.TextView;


import com.brighteyes.gharemonadmin.R;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by bridge on 11/20/2016.
 */

public class CommonUtil implements LocationListener {
    private static final String LOG_TAG = "COMMON_UTIL";

    public static String unixToDateString(Long unix_timestamp) throws ParseException {
        long timestamp = unix_timestamp * 1000;

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String date = sdf.format(timestamp);
        String result = date.toString();
        return result;
    }


    public static Date unixToDate(Long unix_timestamp) throws ParseException {
        long timestamp = unix_timestamp * 1000;

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String dateString = sdf.format(timestamp);
        Date parsedDate = sdf.parse(dateString);

        return parsedDate;
    }


    public static boolean checkDateValid(String startDateString, String endDateString) throws ParseException {
        boolean flag = false;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date strDate = sdf.parse(startDateString);
        Date endDate = sdf.parse(endDateString);
        if (new Date().after(strDate) && new Date().before(endDate)) {
            flag = true;
        } else {
            flag = false;
        }
        return flag;
    }


    public static long getDateDiff(Date date1, Date date2) {
        long diffInMillies = date1.getTime() - date2.getTime();
        return diffInMillies;
    }


    public static String convertTimeMilliToTimeString(Long TimeInMilli) throws ParseException {
        String hrs;
        String mins;
        String secs;
        float hoursFL = TimeInMilli / (1000 * 60 * 60);
        int hours = (int) hoursFL;
        float minutesFL = (TimeInMilli / (1000 * 60)) - (hours * 60);
        int minutes = (int) minutesFL;
        float secondsFl = (TimeInMilli / 1000) - ((hours * 60 * 60) + (minutes * 60));
        int seconds = (int) secondsFl;
        if (hours < 10) {
            hrs = "0" + hours;
        } else {
            hrs = hours + "";
        }

        if (minutes < 10) {
            mins = "0" + minutes;
        } else {
            mins = minutes + "";
        }


        if (seconds < 10) {
            secs = "0" + seconds;
        } else {
            secs = seconds + "";
        }
        String result = hrs + ":" + mins + ":" + secs;
        return result;
    }


    public static Bitmap loadImageFromWeb(String urlString, String fileName) throws ParseException {
        {
            try {
                URL url = new URL(urlString);
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                return bmp;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public static int isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            try {
                URL url = new URL("https://gharemon-b64b9.firebaseio.com/");
                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                urlc.setConnectTimeout(3000);
                urlc.connect();

                return urlc.getResponseCode();

            } catch (MalformedURLException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return 0;
    }


    public static boolean isInternetAvailable(Context context) throws IOException, InterruptedException {
//        String command = "ping -c 1 firebase.google.com";
//        return (Runtime.getRuntime().exec (command).waitFor() == 0);
        ConnectivityManager cn = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cn.getActiveNetworkInfo();
        if (nf != null && nf.isConnected() == true) {

            if (nf.isAvailable()) {
                return true;
            } else {
//                Toast.makeText(context, "Network Available but there is no Internet connection ...", Toast.LENGTH_LONG).show();
                return false;
            }
        } else {
//            Toast.makeText(context, "Network Not Available ... ", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void changeLocale(final Context context, final Locale locale) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                context.getApplicationContext().getResources().updateConfiguration(config, null);
            }
        }, 30);

    }

    public String findMyLocation(Context context) {
        // get location
        String locationString = "";
        boolean canGetLocation = false;
        Location location = null;
        LocationManager locationManager = (LocationManager) context
                .getSystemService(context.LOCATION_SERVICE);

        // getting GPS status
        boolean isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
        boolean isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled) {
            // no network provider is enabled
        } else {
            canGetLocation = true;
            if (isNetworkEnabled) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000L, 0, this);
                if (locationManager != null) {
                    if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    }
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {
                        locationString = location.getLatitude() + "," + location.getLongitude();
                    }
                }
            }
            // if GPS Enabled get lat/long using GPS Services
            if (isGPSEnabled) {
                if (location == null) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {

                            locationString = location.getLatitude() + "," + location.getLongitude();
                        }
                    }
                }
            }
        }
        return locationString;
    }

    public Bitmap transformImageToCircle(Bitmap source) {
        int size = Math.min(source.getWidth(), source.getHeight());

        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;

        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
        if (squaredBitmap != source) {
            source.recycle();
        }

        Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBitmap,
                BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;
        canvas.drawCircle(r, r, r, paint);

        squaredBitmap.recycle();
        return bitmap;
    }


    public static String getFileExtension(File file) {
        String name = file.getName();
        try {
            return name.substring(name.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }


    public static String getUrlExtension(String urlName) {
        try {
            return urlName.substring(urlName.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }

    public static String getFileName(File file) {
        String name = file.getName();
        try {
            return name.substring(name.lastIndexOf("/") + 1);
        } catch (Exception e) {
            return "";
        }
    }




    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }



    public static boolean isValidEmail(String email)
    {
        String expression = "^[\\w\\.]+@([\\w]+\\.)+[A-Z]{2,7}$";
        CharSequence inputString = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputString);
        if (matcher.matches())
        {
            return true;
        }
        else{
            return false;
        }
    }


    public static boolean isValidPhone(String phone)
    {
        String expression = "^([0-9\\+]|\\(\\d{1,3}\\))[0-9\\-\\. ]{3,15}$";
        CharSequence inputString = phone;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputString);
        if (matcher.matches())
        {
            return true;
        }
        else{
            return false;
        }
    }
}
