package com.brighteyes.gharemonadmin.util;

import com.brighteyes.gharemonadmin.activities.entities.City;
import com.brighteyes.gharemonadmin.activities.entities.Country;
import com.brighteyes.gharemonadmin.activities.entities.Deptor;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AmrNabil on 5/19/2017.
 */

public class Singleton {
    public static FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
    public static Country selectedCountry;
    public static List<Country> countryList;
    public static City selectedCity;
    public static List<City> cityList;
    public static List<Deptor> deptorList;
}
